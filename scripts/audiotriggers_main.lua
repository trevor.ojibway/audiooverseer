-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
  local node = getDatabaseNode();
  DB.addHandler(DB.getPath(node, "soundlist"), "onChildDeleted", triggerSoundsUpdate);
  DB.addHandler(DB.getPath(node, "soundlist"), "onChildAdded", triggerSoundsUpdate);
  DB.addHandler(DB.getPath(node, "randomsoundlist"), "onChildDeleted", triggerRandomSoundsUpdate);
  DB.addHandler(DB.getPath(node, "randomsoundlist"), "onChildAdded", triggerRandomSoundsUpdate);

  triggerSoundsUpdate();
  triggerRandomSoundsUpdate();
	update();
end

function onClose()
  local node = getDatabaseNode();
  DB.removeHandler(DB.getPath(node, "soundlist"), "onChildDeleted", triggerSoundsUpdate);
  DB.removeHandler(DB.getPath(node, "soundlist"), "onChildAdded", triggerSoundsUpdate);
  DB.removeHandler(DB.getPath(node, "randomsoundlist"), "onChildDeleted", triggerRandomSoundsUpdate);
  DB.removeHandler(DB.getPath(node, "randomsoundlist"), "onChildAdded", triggerRandomSoundsUpdate);
end

function triggerSoundsUpdate()
  local node = getDatabaseNode();
  local bHasSounds = (DB.getChildCount(node,"soundlist") > 0);
  subwindow_soundslist.subwindow.play_label.setVisible(bHasSounds);
  subwindow_soundslist.subwindow.stopplay_label.setVisible(bHasSounds);
  subwindow_soundslist.subwindow.delay_label.setVisible(bHasSounds);
end

function triggerRandomSoundsUpdate()
  local node = getDatabaseNode();
  local bHasRandoSounds = (DB.getChildCount(node,"randomsoundlist") > 0);
  subwindow_randomsoundslist.subwindow.random_play_label.setVisible(bHasRandoSounds);
  subwindow_randomsoundslist.subwindow.random_stopplay_label.setVisible(bHasRandoSounds);
end

function updateControl(sControl, bReadOnly, bForceHide)
	if not self[sControl] then
		return false;
	end
		
	return self[sControl].update(bReadOnly, bForceHide);
end

function update()
	local nodeRecord = getDatabaseNode();
	local bReadOnly = WindowManager.getReadOnlyState(nodeRecord);

	updateControl("type", bReadOnly);
	updateControl("match", bReadOnly);
	updateControl("sound", bReadOnly);
  
  -- matches_list
  hideDeleteButton(matches_list,bReadOnly)
  -- sounds_list
  hideDeleteButton(subwindow_soundslist.subwindow.sounds_list,bReadOnly)
  -- randomsounds_list
  hideDeleteButton(subwindow_randomsoundslist.subwindow.randomsounds_list,bReadOnly)

  match_add.setVisible(not bReadOnly);
  
end

function hideDeleteButton(window_list,bReadOnly)
  for _,w in ipairs(window_list.getWindows()) do
    w.idelete.setVisible(not bReadOnly);
    if w.name then
      w.name.setReadOnly(bReadOnly);
    end
    if w.delay then
      w.delay.setReadOnly(bReadOnly);
    end
    if w.match then
      w.match.setReadOnly(bReadOnly);
    end
  end
end
