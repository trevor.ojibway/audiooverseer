-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
  local node = getDatabaseNode();
  DB.addHandler(DB.getPath(node, "collectionsoundlist"), "onChildDeleted", collectionsUpdate);
  DB.addHandler(DB.getPath(node, "collectionsoundlist"), "onChildAdded", collectionsUpdate);
  collectionsUpdate();
	update();
end

function onClose()
  local node = getDatabaseNode();
  DB.removeHandler(DB.getPath(node, "collectionsoundlist"), "onChildDeleted", collectionsUpdate);
  DB.removeHandler(DB.getPath(node, "collectionsoundlist"), "onChildAdded", collectionsUpdate);
end

function onDrop(x, y, draginfo)
local node = getDatabaseNode();
  if not WindowManager.getReadOnlyState(node) then
    if draginfo.isType("shortcut") then
      local sClass, sRecord = draginfo.getShortcutData();
      if sClass == 'audio' then
        local nodeSounds = node.createChild("collectionsoundlist");
        local nodeSound = nodeSounds.createChild();
        if nodeSound then
          local nodeSource = DB.findNode(sRecord);
          local sName = DB.getValue(nodeSource,"name","");
          DB.setValue(nodeSound,"sound","string",sRecord);
          DB.setValue(nodeSound,"name","string",sName);
        end
      end
    end
    return true;
  end
end


function collectionsUpdate()
  local node = getDatabaseNode();
  local bHasCollections = (DB.getChildCount(node,"collectionsoundlist") > 0);
  play_label.setVisible(bHasCollections);
  stopplay_label.setVisible(bHasCollections);
end

function updateControl(sControl, bReadOnly, bForceHide)
	if not self[sControl] then
		return false;
	end
		
	return self[sControl].update(bReadOnly, bForceHide);
end

function update()
	local nodeRecord = getDatabaseNode();
	local bReadOnly = WindowManager.getReadOnlyState(nodeRecord);

	updateControl("type", bReadOnly);
  -- collectionsounds_list
  hideDeleteButton(collectionsounds_list,bReadOnly)
  
end

function hideDeleteButton(window_list,bReadOnly)
  for _,w in ipairs(window_list.getWindows()) do
    w.idelete.setVisible(not bReadOnly);
    if w.name then
      w.name.setReadOnly(bReadOnly);
    end
  end
end