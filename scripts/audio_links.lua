-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

-- Any place we have start/stop audio links this script is applied to manage them.

function onInit()
  updateSoundLinks()
end

function onValueChanged()
updateSoundLinks();
end

-- update sound links play/stop with current records accounting for "module" paths.
function updateSoundLinks()
  local node = window.getDatabaseNode();
  local sSoundRoot = DB.getValue(node,"sound","");
  if (not sSoundRoot:find("@")) then
    sSoundRoot = sSoundRoot .. "@*";  
  end  
  window.audioshortcut.setValue("urlaudio",sSoundRoot);
  window.audiostopshortcut.setValue("urlaudiostop",sSoundRoot);
end

-- light up the text on hover
function onHover(bOnControl)
  if bOnControl then
    setColor("#FFFFFF");
  else
    setColor(nil);
  end
end

-- if they click the name of the sound, bring up the record window for that record.
function onClickDown(button,x,y)
  local node = window.getDatabaseNode();
  local sSoundRoot = DB.getValue(node,"sound","");
  if (not sSoundRoot:find("@")) then
    sSoundRoot = sSoundRoot .. "@*";  
  end  
  local w = Interface.openWindow("audio",sSoundRoot);
  return true;
end
