-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
end


function createBlankAudioRecord(sNameIt)
  local node = nil;
  if not sNameIt or sNameIt == "" then 
    return DB.createChild("audio");
  end
  local bUpdate = (update_checkbox.getValue() == 1);
  
  -- this shouldn't check findNode if not bUpdate... maybe improve speed?
  if bUpdate and DB.findNode("audio." .. sNameIt) then
    -- Debug.console("table_import.lua","createBlankAudioRecord","Skipping existing record:",sNameIt);    
  else
    sNameIt = getUniqueName(sNameIt);
    node = DB.createChild("audio",sNameIt); 
  end

  return node;
end

function importTextAsAudio(sClipboardText)
  local bFGC = (Interface.getVersion() < 4);
  local _MAX = 50;
  if not bFGC then
    _MAX = 100;
  end
  local nTotalCounter = totalcounter.getValue();
  local nTotalProcessCounter =totalprocessedcounter.getValue();

  local sText = nil;
  if sClipboardText and sClipboardText ~= "" then
    sText = sClipboardText;
  else
    sText = importtext.getValue() or "";
  end
--Debug.console("table_import.lua","importTextAsAudio","sText",sText);    
  if (sText ~= "") then
    local aImportText = {};
    local aImportTextRemaining = {};
    for sLine in string.gmatch(sText, '([^\r\n]+)') do
      sLine = StringManager.trim(sLine);  -- remove spaces in front and on end.
      table.insert(aImportText, sLine);
    end
    local nCounter = 0;
    local bReachedMax = false;
    for _,sImportLine in ipairs(aImportText) do
      -- dont let FGC process more then _MAX at a wack or it'll crash more than likely.
      if (nCounter >= _MAX) then
        if nCounter == _MAX then
          if not bReachedMax then
            ChatManager.Message("*** AudioOverseer: Max import count reached.\rImport no more the ".. _MAX .. " at a time. If import window is still showing, click IMPORT and it will process the remaining entries up to " .. _MAX .. " at a time.",true,nil);
            Debug.console("--- Reached _MAX",_MAX);
            bReachedMax = true;
          end
        end
        table.insert(aImportTextRemaining, sImportLine);
      else
        nTotalProcessCounter = nTotalProcessCounter + 1;
        -- local aValues = split(sImportLine,",");
        -- sImportLine = sImportLine:gsub("\"\"", '"');
        local aValues = patternMatchSplit(sImportLine,"\"(.-)\",");
        local aLastValue = split(sImportLine,"\"(.-)\",");
        table.insert(aValues,stripQuotes(aLastValue[#aLastValue]));
        -- Debug.console("audio_import.lua","importTextAsAudio","aValues",aValues);                
        --status, subcategory, product_or_pack, soundset, name, type, sub_type, genre_players_play_url
        if #aValues >= 9 then
          local sID         = cleanText(aValues[1]);
          local sStatus     = cleanText(aValues[2]);
          local sCategory   = cleanText(aValues[3]);
          local sPack       = cleanText(aValues[4]);
          local sSet        = cleanText(aValues[5]);
          local sName       = cleanNameText(aValues[6]);
          local sType       = cleanText(aValues[7]);
          local sSubType    = cleanText(aValues[8]);
          local sPath       = cleanText(aValues[9]);
          local sPath_Stop  = cleanText(aValues[10] or "");
          local sPath2      = cleanText(aValues[11] or "");
          local sPath2_Stop = cleanText(aValues[12] or "");
          
          --local sNodeName = sCategory .. sPack .. sSet .. sType .. sSubType .. sName;
          
          -- this one will rename duplicates
          -- sNodeName = getUniqueName(alphaNumberOnly(sNodeName));
          
          -- update existing records of the same name
          --sNodeName = alphaNumberOnly(sNodeName);
          local sNodeName = alphaNumberOnly(sID);
          --
          
          local nodeAudio = createBlankAudioRecord(sNodeName);
          if nodeAudio then
            nCounter = nCounter + 1;
            nTotalCounter = nTotalCounter + 1;
            DB.setValue(nodeAudio,"id","string",sID);
            DB.setValue(nodeAudio,"status","string",sStatus);
            DB.setValue(nodeAudio,"category","string",sCategory);
            DB.setValue(nodeAudio,"pack","string",sPack);
            DB.setValue(nodeAudio,"set","string",sSet);
            DB.setValue(nodeAudio,"name","string",sName);
            DB.setValue(nodeAudio,"type","string",sType);
            DB.setValue(nodeAudio,"subtype","string",sSubType);
            DB.setValue(nodeAudio,"path","string",sPath);
            DB.setValue(nodeAudio,"path_stop","string",sPath_Stop);

            DB.setValue(nodeAudio,"path2","string",sPath2);
            DB.setValue(nodeAudio,"path2_stop","string",sPath2_Stop);


            Debug.console("audio_import.lua","importTextAsAudio","Added " .. sSet .. "/" .. sName);
            DB.setValue(nodeAudio,"locked","number",1);
            ChatManager.SystemMessage("AudioOverseer imported " .. sName .. " into " .. sCategory .. "/" .. sSet .. " .");
          else
            Debug.console("audio_import.lua","importTextAsAudio","Skipped existing:",sCategory .. "/" .. sPack .. "/" .. sSet .. "/" .. sType .. "/" .. sSubType .. "/" .. sName);
          end
        else
          Debug.console("audio_import.lua","importTextAsAudio","Number of values is not greater or equal to 9. (#aValues):",aValues);
        end
      end
    end
    importtext.setValue("");
    if #aImportTextRemaining > 0 then
      import_clipboard_button.setVisible(false);
      import_button.setVisible(true);
      import_button.setText("IMPORT(" .. _MAX .. ")");
      import_helptext.setValue(nTotalProcessCounter .. " total records processed with " .. #aImportTextRemaining .. " remaining.\n\rPress IMPORT to process next batch.\n\r");
      importtitle.setValue("IMPORT NEXT ".. _MAX);
      totalcounter.setValue(nTotalCounter);
      totalprocessedcounter.setValue(nTotalProcessCounter);
      importtext.setValue(table.concat(aImportTextRemaining,"\r\n"));
    else
      ChatManager.SystemMessage("*** AudioOverseer: Imported ".. nTotalCounter .." records. Work Complete. ");
      -- close window
      close();
    end
  end
end

-- use this to capture "field","field","field" CSV but if you wanna match on $ it wont
function patternMatchSplit(sText,sPattern)
  local aFields = {};
  for w in string.gmatch(sText, sPattern) do
    table.insert(aFields,w);
  end  
  return aFields;
end

-- remove leading/trailing spaces/extra quotes
function cleanText(sText)
  -- remove spaces
  sText = StringManager.trim(sText);
  return sText;
end

-- clean name text
function cleanNameText(sText)
  sText = sText:gsub("\"\"", '"')
  -- remove spaces
  sText = StringManager.trim(sText);
  return sText;
end

-- remove quotes
function stripQuotes(sText)
  sText = sText:gsub("[\"]+", '')
  return sText;
end
-- return only alpha/number characters in sString
function alphaNumberOnly(sString)
  return sString:gsub("[^a-zA-Z0-9]+","");
end

-- this makes sure we get a unique name if they don't want to overwrite the current one.
function getUniqueName(sName)
  local nCounter = 1;
  while (DB.findNode("audio." .. sName)) do
    local sNumberOf = sName:match("(%d+)$");
    nCounter = nCounter + 1;
    if sNumberOf then
      local nNumberOf = (tonumber(sNumberOf) or 0);
      nCounter = nNumberOf + 1;
      sName = sName:gsub("%d+$","");
    end
    sName = sName .. nCounter
  end
  
  return sName;
end

function split(str, pat)
  local t = {};
  local fpat = "(.-)" .. pat;
  local last_end = 1;
  local s, e, cap = str:find(fpat, 1);
  while s do
  if s ~= 1 or cap ~= "" then
   table.insert(t,cap);
  end
    last_end = e+1;
    s, e, cap = str:find(fpat, last_end);
  end
  if last_end <= #str then
    cap = str:sub(last_end);
    table.insert(t, cap);
  end
  return t
end