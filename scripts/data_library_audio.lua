-- 
-- Please see the license.html file included with this distribution for 
-- attribution and copyright information.
--

local DEFAULT_STOP_SOUND_NAME="STOP All Sounds (Syrinscape Online Player)";
local DEFAULT_STOP_SOUND_CATEGORY="General";
local DEFAULT_STOP_SOUND_NODE="syringscapeonlineplayerglobalstop";
local DEFAULT_STOP_SOUND_PATH1="syrinscape-fantasy:stop/";
local DEFAULT_STOP_SOUND_PATH2="https://syrinscape.com/online/frontend-api/stop-all/";
local DEFAULT_STOP_SOUND_NOTE="This sound link will stop all sounds played by the Syrinscape Player."

function onInit()
  
  if User.isHost() then
    local sRuleset = Interface.getRuleset();
    local sButtonTag;
    if sRuleset == "2E" or sRuleset == "5E" then
      sButtonTag = sRuleset;
    else
      sButtonTag = 'default';
    end
    
    aRecords = {
      ["audio"] = {
        bExport = true, 
        aDataMap = { "audio", "reference.audiodata" }, 
        aDisplayIcon = { "button_sidebar_" .. sButtonTag, "button_sidebar_down_" .. sButtonTag},
        sRecordDisplayClass = "audio", 
        aGMEditButtons = { "button_import_audio" };
        aGMListButtons = { "button_audiotriggers","button_audiocollections" };
        aCustomFilters = {
          ["Category"] = { sField = "category", sType = "string" },
          ["Pack"] = { sField = "pack", sType = "string" },
          ["Sound Set"] = { sField = "set", sType = "string" },
          ["Type"] = { sField = "type", sType = "string" },
          ["Sub-Type"] = { sField = "subtype", sType = "string" },
        },
      },
      ["audiotriggers"] = {
        bHidden = true,
        bExport = true, 
        aDataMap = { "audiotriggers", "reference.audiotriggersdata" }, 
        aDisplayIcon = { "button_sidebar_" .. sButtonTag, "button_sidebar_down_" .. sButtonTag},
        sRecordDisplayClass = "audiotriggers", 
        aCustomFilters = {
          ["Type"] = { sField = "type", sType = "string" },
        },
      },
      ["audiocollections"] = {
        bHidden = true,
        bExport = true, 
        aDataMap = { "audiocollections", "reference.audiocollectionsdata" }, 
        aDisplayIcon = { "button_sidebar_" .. sButtonTag, "button_sidebar_down_" .. sButtonTag},
        sRecordDisplayClass = "audiocollections", 
        aCustomFilters = {
          ["Type"] = { sField = "type", sType = "string" },
        },
      },
    }

    -- insert out aRecords here:
    for kRecordType,vRecordType in pairs(aRecords) do
      LibraryData.setRecordTypeInfo(kRecordType, vRecordType);
    end
    
    -- make sure that this node exists to ensure we have a STOP link.
    if not DB.findNode("audio." .. DEFAULT_STOP_SOUND_NODE) then
      local nodeAudioRoot = DB.createNode("audio");
      local nodeAudio = DB.createChild(nodeAudioRoot,DEFAULT_STOP_SOUND_NODE);
      DB.setValue(nodeAudio,"name","string",DEFAULT_STOP_SOUND_NAME);
      DB.setValue(nodeAudio,"category","string",DEFAULT_STOP_SOUND_CATEGORY);
      DB.setValue(nodeAudio,"path","string",DEFAULT_STOP_SOUND_PATH1);
      DB.setValue(nodeAudio,"path_stop","string",DEFAULT_STOP_SOUND_PATH1);
      DB.setValue(nodeAudio,"path2","string",DEFAULT_STOP_SOUND_PATH2);
      DB.setValue(nodeAudio,"path2_stop","string",DEFAULT_STOP_SOUND_PATH2);
      DB.setValue(nodeAudio,"notes","formattedtext",DEFAULT_STOP_SOUND_NOTE);
      DB.setValue(nodeAudio,"locked","number",1);
    end
  end
end
